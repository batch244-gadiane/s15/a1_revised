


	let firstName = " John";
	console.log("First Name: "+firstName);
	let lastNAme = "Smith"
	console.log("Last Name: "+ lastNAme);
	let age = "30";
	console.log("Age: " + age);
	let hobby = 'Hobbies: ';
	console.log(hobby);
	let hobbies = ["Hiking","Basketball","Playing Guitar"];
	console.log(hobbies);
	let WorkAdress = "Work Address:"
	console.log(WorkAdress);
	let address = {
		houseNumber: 32,
		street: "Washington",
		city: "Lincoln",
		state: 'Nebraska'
	}
	console.log( address);
/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let ages = 40;
	console.log("My current age is: " + ages);

	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
		console.log("My friends are: ");
	console.log(friends);
	

	let profile = {

		username: 'captain_'+'america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false,

	}

	console.log("My Full Profile: ");
	console.log(profile);
	//console.log(profile);

	let fullNames = "Bucky Barnes";
	console.log("My bestfriend is: " + fullNames);

	const lastLocation = "Arctic Ocean";
	lastLocation1 = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);

